import { createSimpleLogger } from 'simple-node-logger';
import { createQueue } from '@manganese/queue-kit';
import { Configuration, Context } from './types';
import {
  OwntracksIngestionService,
  RadiationIngestionService,
} from './services';
import { Server } from './Server';

export class Application {
  private readonly context: Context;

  constructor(private readonly configuration: Configuration) {
    const log = createSimpleLogger();
    const queue = createQueue(configuration.queue);
    const context = (this.context = {
      log,
      queue,
      services: {
        owntracksIngestionService: null,
        radiationIngestionService: null,
      },
      server: null,
    });
    context.services.owntracksIngestionService = new OwntracksIngestionService(
      context
    );
    context.services.radiationIngestionService = new RadiationIngestionService(
      context
    );
    context.server = new Server(configuration.server, context);
  }

  public async initialize() {
    await this.context.queue.initialize();
  }

  public start() {
    if (this.configuration.server.port) {
      this.context.server?.listen();
    }
  }

  public get expressApplication() {
    return this.context.server.getExpressApplication();
  }
}
