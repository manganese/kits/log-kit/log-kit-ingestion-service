import express, { Express, Request } from 'express';
import bodyParser from 'body-parser';
import {
  OwntracksPayload,
  RadiationPayload,
  isValidCpm,
} from '@manganese/log-kit-core';
import { Context, ServerConfiguration } from './types';

export class Server {
  private readonly expressApplication: Express;

  constructor(
    private readonly configuration: ServerConfiguration,
    private readonly context: Context
  ) {
    const expressApplication = (this.expressApplication = express());
    expressApplication.use(bodyParser.json());
    expressApplication.use(bodyParser.urlencoded({ extended: true }));

    expressApplication.post(
      '/ingest/owntracks',
      async (request: Request<{}, null, OwntracksPayload>, response, next) => {
        try {
          const payload = request.body;

          await this.context.services.owntracksIngestionService.handlePayload(
            payload
          );

          response.sendStatus(200);

          next();
        } catch (error) {
          next(error);
        }
      }
    );

    expressApplication.get(
      '/ingest_radiation',
      async (
        request: Request<
          {},
          null,
          null,
          { AID: string; GID: string; CPM: string; uSV: string }
        >,
        response,
        next
      ) => {
        try {
          const sensorId = 'prima-rad1';
          const cpmString = request.query.CPM;
          const cpmNumber =
            typeof cpmString === 'string' ? parseFloat(cpmString) : null;

          if (!isValidCpm(cpmNumber)) {
            response.sendStatus(400);

            return next();
          }

          const cpm = cpmNumber as number;
          const payload: RadiationPayload = {
            AID: request.query.AID,
            GID: request.query.GID,
            CPM: cpm,
          };

          await this.context.services.radiationIngestionService.handlePayload(
            payload
          );

          response.sendStatus(200);

          next();
        } catch (error) {
          next(error);
        }
      }
    );
  }

  public listen() {
    this.expressApplication.listen(this.configuration.port);
  }

  public getExpressApplication() {
    return this.expressApplication;
  }
}
