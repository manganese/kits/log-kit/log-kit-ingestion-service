#!/usr/bin/env node
import yargs from 'yargs';
import { Application, parseConfigurationJson, validateConfiguration } from '..';

yargs
  .scriptName('log-kit-ingestion-service')
  .usage('$0 <command> [arguments]')
  .command<{
    configurationJson: string;
  }>(
    'start',
    'Start a LogKit ingestion service',
    (yargs) => {
      yargs.option('configuration-json', {
        type: 'string',
        required: true,
        describe: 'Configuration JSON',
      });
    },
    async (argv) => {
      const configuration = parseConfigurationJson(argv.configurationJson);

      try {
        validateConfiguration(configuration);

        const application = new Application(configuration);

        await application.initialize();

        application.start();
      } catch (error) {
        console.error(error);
      }
    }
  )
  .help().argv;
