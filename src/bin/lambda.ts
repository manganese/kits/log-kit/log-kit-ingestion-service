import serverlessExpress from '@vendia/serverless-express';
import { CONFIGURATION_ENVIRONMENT_VARIABLE } from '../constants';
import { parseConfigurationJson, validateConfiguration } from '../utilities';
import { Application } from '../Application';

let serverlessExpressInstance: any;
let initializationPromise: Promise<void>;

async function createAndInitialize() {
  try {
    const configurationString = process.env[CONFIGURATION_ENVIRONMENT_VARIABLE];
    const configuration = parseConfigurationJson(configurationString);

    validateConfiguration(configuration);

    const application = new Application(configuration);

    // await application.initialize();

    serverlessExpressInstance = serverlessExpress({
      app: application.expressApplication,
    });
  } catch (error) {
    console.error(error);
  }
}

export async function handler(event: any, context: any) {
  if (!serverlessExpressInstance) {
    if (!initializationPromise) {
      initializationPromise = createAndInitialize();
    }

    await initializationPromise;
  }

  return serverlessExpressInstance(event, context);
}
