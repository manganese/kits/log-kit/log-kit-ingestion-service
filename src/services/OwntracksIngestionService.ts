import {
  MessageType,
  OwntracksLocationPayload,
  OwntracksMessage,
  OwntracksPayload,
  OwntracksPayloadType,
  getOwntracksLocationPayloadIds,
} from '@manganese/log-kit-core';
import { Context } from '../types';

export class OwntracksIngestionService {
  constructor(private readonly context: Context) {}

  public async handlePayload(payload: OwntracksPayload) {
    switch (payload._type) {
      case OwntracksPayloadType.Location:
        await this.handleLocationPayload(payload as OwntracksLocationPayload);

        break;
      default:
        this.context.log.info(`Ignored OwnTracks ${payload._type} payload`);

        break;
    }
  }

  private async handleLocationPayload(payload: OwntracksLocationPayload) {
    const message: OwntracksMessage = {
      type: MessageType.Owntracks,
      payload,
    };
    const { userId, deviceId } = getOwntracksLocationPayloadIds(payload);

    await this.context.queue.publish(message);

    this.context.log.info(
      `Handled OwnTracks location payload for ${userId}:${deviceId}`
    );
  }
}
