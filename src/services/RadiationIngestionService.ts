import {
  MessageType,
  RadiationMessage,
  RadiationPayload,
} from '@manganese/log-kit-core';
import { Context } from '../types';

export class RadiationIngestionService {
  constructor(private readonly context: Context) {}

  public async handlePayload(payload: RadiationPayload) {
    const message: RadiationMessage = {
      type: MessageType.Radiation,
      payload,
    };

    await this.context.queue.publish(message);

    this.context.log.info(
      `Handled radiation payload for ${payload.AID}:${payload.GID} of ${payload.CPM}CPM`
    );
  }
}
