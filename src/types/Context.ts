import { Logger } from 'simple-node-logger';
import { Queue } from '@manganese/queue-kit';
import {
  OwntracksIngestionService,
  RadiationIngestionService,
} from '../services';
import { Server } from '../Server';

export interface Context {
  log: Logger;
  queue: Queue;
  services: {
    owntracksIngestionService: OwntracksIngestionService;
    radiationIngestionService: RadiationIngestionService;
  };
  server: Server;
}
