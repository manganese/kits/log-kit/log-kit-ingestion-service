import { QueueConfiguration } from '@manganese/queue-kit';
import { ServerConfiguration } from '.';

export interface Configuration {
  queue: QueueConfiguration;
  server: ServerConfiguration;
}
