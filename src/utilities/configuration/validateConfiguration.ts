import { Configuration } from '../../types';

export const validateConfiguration = (configuration: Configuration) => {
  if (!configuration.queue) {
    throw new Error('Configuration must specify a queue');
  }
};
